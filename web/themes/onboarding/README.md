Onboarding Theme
-
The files contained in this repo provide a starting point for developing a Onboarding theme. It is a subtheme of Classy.

Consider this a work in progress. There is much that needs to be added and finished.

There are many concepts at play here to allow you to create a custom theme using tools for performance, accessibility, testing, and fast development.

This repo includes:

- Gulp for task-running
- Sass for pre-processing CSS, using cleanCSS
- Sourcemaps for connecting a minified/uglified version of an asset (CSS or JavaScript) to the original authored version
- Autoprefixer allows you to choose browser versions and helps you add or remove vendor prefix for only the browsers you need to have.
- concat method is used to merge two or more JS files into one
- uglify that compresses the file size by removing all the spaces and new lines which makes the code unreadable able hence ugly.
- environments that restrict code to be functional on development site only or production site only
- And much more theme development goodness
- --
Pre-installation Notes

The installation steps below presume you have already installed Drush, Node.js and npm on your system. If you have never used those before or are certain if they are available on your system, follow these installation instructions. Otherwise, you can skip to the installation instructions below.

first install node version v17.3.0 which includes npm 8.3.0: https://nodejs.org/download/release/v17.3.0

cd yourThemeRoot directory

after installing node and npm you need to install gulp version 4.0.2 with composer

npm install -g gulp@v4.0.2

Check versions<br>
npm -v<br>
node -v<br>
gulp -v


---
Installation Instructions

We are assuming here you have already installed Drupal 8 and it is running on your development server. To begin installing this theme, navigate to the themes directory (cd themes). If you wish, you can add a new directory for your custom themes (mkdir custom && cd custom).


Where you see in these instructions your_theme you should substitute the name of your theme, using all lower case and with no spaces. Use underscores to separate words.

Now install the theme files using:
git clone https://RobertArtic@bitbucket.org/robertartic/drupal.git  your_theme

Change directories to where your put your theme.

cd your_theme
Rename the theme info.yml file.

mv onboarding.info.yml your_theme.info.yml

Edit your_theme.info.yml to reflect your theme's name and other details. Make sure you change the path under "# Locate files" to reflect your theme directory's name. See also "Defining a theme with an .info.yml file".

Rename the libraries.yml file.

mv onboarding.libraries.yml your_theme.libraries.yml

Editing may not be needed in your_theme.libraries.yml, or at least not right away. This is where you can add other CSS and JS files. You can find more information on the use of the libraries.yml file [here] (https://www.drupal.org/developing/api/8/assets).

Now install all of the Node.js modules we need. (This will take a while.)

npm install

You're now ready to visit your Drupal site and enable the new theme.

Post-installation Notes
Running npm install and gulp install will add several files in directories called node_modules. The .gitignore file in your theme will prevent these files from being added to your repo. This is intentional because the files are only needed for development.

If you are adding developers on a team who are editing the theme, after they have cloned your site's repo they will need to navigate to the theme directory and run these commands:

npm install

As well as

npm run dev (for development environment)
npm run prod (for production environment)

For development
The development of a theme using these files and configuation would normally proceed as follows:


Use Gulp commands to automate the development process. Before you beging writing files, use the command npm run dev to start up the full task-running process. Gulp will watch for file changes, process Sass files into CSS, trigger Drush to clear cache when Drupal template files are changed, and refresh the browser.

Gulp will watch for changes in your Sass files automatically process into CSS

Use control-c to stop Gulp watch tasks. When you use the command npm run dev to start development, watch tasks will continue to run. When you wish to stop these processes, use the keyboard command control-c (assuming you are developing on a Mac). This will stop all tasks.

Put theme template files in /templates. This directory already contains several template files, lifted directly from Classy theme. Use these or add new ones within this file structure.

Extra time is needed before the browser refreshes when you change template files. When you make changes to Drupal template files or add new files, a Gulp task will clear Drupal cache and refresh the browser. Drupal requires that cached is cleared after template change, so a Drush command is added to the the watch sequence for template files. This step takes longer to run than other changes, so expect to wait at least 15 seconds before your changes are reflected in the browser. (See this issue for more details.)
