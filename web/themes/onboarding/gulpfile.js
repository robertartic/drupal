const {watch, series, src, dest} = require('gulp');
const sass = require('gulp-sass')(require('sass'));
const sourcemaps = require('gulp-sourcemaps');
const autoprefixer = require('gulp-autoprefixer');
const concat = require('gulp-concat');
const uglify = require('gulp-uglify');
const cleanCSS = require('gulp-clean-css');
const environments = require('gulp-environments');
const development = environments.development;
const production = environments.production;
const gulpStylelint = require('gulp-stylelint');
const clean = require('gulp-clean');

function css() {
  return src('./src/sass/**/*.scss')
    .pipe(development(gulpStylelint({
      failAfterError: false,
      reporters: [
        {formatter: 'string', console: true}
      ]
    })))
    .pipe(development(sourcemaps.init()))
    .pipe(sass().on('error', sass.logError))
    .pipe(production(cleanCSS({compatibility: 'ie8'})))
    .pipe(production(autoprefixer()))
    .pipe(development(sourcemaps.write('../css')))
    .pipe(dest('./dist/css'))
}

function js() {
  return src('./src/js/**/*.js')
    .pipe(development(sourcemaps.init()))
    .pipe(production(uglify()))
    .pipe(development(sourcemaps.write('../js')))
    .pipe(dest('./dist/js'));
}

function destroy() {
  return src(['./dist/js/*.js','./dist/js/components/*.js', './dist/css/*.css','./dist/css/components/*.css', './dist/css/*.map','./dist/css/components/*.map', './dist/js/*.map','./dist/js/components/*.map'])
    .pipe(clean())
}

function watchFiles() {
  watch('./src/sass/**/*.scss', {usePolling: true}, css);
  watch(['./src/js/*.js', '!js/*.min.js'], {usePolling: true}, js);

}

//delete, compile and watch files
exports.prod = series(destroy, css, js);
exports.dev = series(destroy, css, js, watchFiles);
