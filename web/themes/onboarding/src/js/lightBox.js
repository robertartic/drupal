(function (Drupal, once, $) {
  Drupal.behaviors.lightbox = {
    attach(context) {
      var elements = once(
        "gallery",
        ".paragraph--type--gallery",
        context
      );

      for (var i = 0; i < elements.length; i++) {
        var content = elements[i];
        var slideIndex;


        // Thumbnail image controls
        function currentSlide(n) {
          showSlides(slideIndex = n);
        }

        //select images in gallery
        var images = document.querySelectorAll(".field__item.hover-shadow");

        //looping through images and adding event listener - click
        images.forEach((image) => {

          //open modal on click
          image.addEventListener('click', () => {
            document.getElementById("myModal").style.display = "flex";
            document.querySelector("body").classList.add("stop-scrolling");
            //gets a number from id that determines its order
            var imageId = image.id;
            //only select numbers
            slideIndex = imageId.match(/\d/g);
            slideIndex = Number.parseInt(slideIndex.join(""));
            //number that select proper image to show in gallery
            showSlides(slideIndex);
          });
        });

        //Close modal
        var closeModal = content.querySelector(".close");
        if (closeModal) {
          closeModal.addEventListener("click", function () {
            document.getElementById("myModal").style.display = "none";
            document.querySelector("body").classList.remove("stop-scrolling");
          })
        }

        //thumbnail section
        var thumbnailImages = content.querySelectorAll(".demo");
        thumbnailImages.forEach((thumbnailImage, index) => {
          thumbnailImage.addEventListener('click', () => {
            currentSlide(index + 1);
          })
        })


        function showSlides(n) {
          var i;
          var slides = document.getElementsByClassName("mySlides");
          var dots = document.getElementsByClassName("demo");

          if (n > slides.length) {
            slideIndex = 1
          }
          if (n < 1) {
            slideIndex = slides.length
          }
          for (i = 0; i < slides.length; i++) {
            slides[i].classList.remove("active");
          }
          for (i = 0; i < dots.length; i++) {
            dots[i].className = dots[i].className.replace(" active", "");
          }
          //slides[slideIndex - 1].style.display = "flex";
          slides[slideIndex - 1].classList.add("active");
          dots[slideIndex - 1].className += " active";
        }

        //next slide
        var nextSlide = document.querySelector(".next");
        nextSlide.addEventListener("click", function () {
         slideIndex = slideIndex + 1;
          showSlides(slideIndex);
        })

        //previous slide
        var prevSlide = document.querySelector(".prev");
        prevSlide.addEventListener("click", function () {
          slideIndex = slideIndex - 1;
          showSlides(slideIndex);
        })
      }
    },

  };
})(Drupal, once, jQuery);
