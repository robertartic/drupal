(function (Drupal, once, $) {
  Drupal.behaviors.news = {
    attach(context) {
      var elements = once("news", ".layout-content", context);

      for (var i = 0; i < elements.length; i++) {
        var content = elements[i];

        const element = content.querySelector('.field--name-field-image');
        const target = content.querySelector("#block-onboarding-page-title");
        target.parentNode.insertBefore(element, target)
      }
    },
  };
})(Drupal, once, jQuery);
