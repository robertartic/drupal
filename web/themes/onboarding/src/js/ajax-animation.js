(function (Drupal, once, $) {

  $(document).ready(function ($) {
    $('.views-infinite-scroll-content-wrapper').find('.views-row').addClass('no-animation');
  });

  Drupal.behaviors.myCustomBehavior = {
    attach: function (context, settings) {
      $('.views-infinite-scroll-content-wrapper', context).find('.views-row').each(function (index) {
        $(this).addClass('animation');
      });
    }
  };
})(Drupal, once, jQuery);
