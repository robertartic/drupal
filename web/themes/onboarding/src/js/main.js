(function (Drupal, once, $) {
  Drupal.behaviors.main = {
    attach(context) {
      var elements = once("main", ".region-header", context);

      for (var i = 0; i < elements.length; i++) {
        var header = elements[i];
        let overlay = document.querySelector(".nav");
        let open = document.querySelector(".fa-bars");
        let close = document.querySelector(".fa-times");


        window.addEventListener('load', function () {
          var icon = header.querySelector("#nav-icon");
          icon.addEventListener("click", function () {
            icon.classList.toggle("open");
            overlay.classList.toggle("open");
          })
        }, false);


        //upcoming events - homepage
        let viewportWidth = window.innerWidth;

        if (viewportWidth > 768) {
          console.log("hahaha");
          const events = document.querySelectorAll(".upcoming-events .events-row .views-field-view-node a");
          const eventsImage = document.querySelectorAll(".upcoming-events .events-gallery-row");
          let imageIndex;

          events.forEach((event, index) => {
            event.addEventListener('mouseover', () => {
              //gets a number from id that determines its order
              imageIndex = index;
              showImages(imageIndex);
            });
          });

          function showImages(n) {
            if (n > eventsImage.length) {
              imageIndex = 1;
            }
            if (n < 1) {
              imageIndex = eventsImage.length
            }
            for (i = 0; i < eventsImage.length; i++) {
              eventsImage[i].style.display = "none";
              eventsImage[n].style.display = "block";
            }
          }
        }

        // show more function on content types body
        const showMore = document.createElement("button");
        const text = document.createTextNode("SHOW MORE");
        showMore.appendChild(text);
        showMore.value = "SHOW MORE";
        showMore.className = "showMore";
        const paragraphs = document.querySelectorAll('.node__content .field--name-body p');
        const paragraphsParent = document.querySelector('.node__content .field--name-body');
        let length = paragraphs.length;

        //only select other that first element and add class to them
        for (let i = 1; i < length; i++) {
          paragraphs[i].classList.add("paragraph-hidden");

          //appending button to parent
          paragraphsParent.appendChild(showMore);
        }

        if (length === 1) {

        }
        showMore.addEventListener('click', () => {
          for (let j = 1; j < length; j++) {
            paragraphs[j].classList.toggle("paragraph-hidden");
          }
          showMore.classList.toggle("open");

          if (showMore.innerHTML === "SHOW MORE") {
            showMore.innerHTML = "SHOW LESS";
          } else {
            showMore.innerHTML = "SHOW MORE";
          }
        });

      }
    },

  };
})(Drupal, once, jQuery);
